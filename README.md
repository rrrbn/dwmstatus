# dwmstatus

dwm status (foo)bar in go

## usage

	go install bitbucket.org/rrrbn/dwmstatus

put `tools/vol` to somewhere in your `PATH`. bind volume changing keys to `vol up`, `vol down` or `vol mute`.
the `vol` script will change volume via calling the matching pactl magic and run dwmstatus one time to update
the volume level display.

launch dwmstatus with `-oneshot=false` to have it running in background, sleeping for 30s between updates
of date/time/battery display.

this is quite hacky, but works without any additional magick.
