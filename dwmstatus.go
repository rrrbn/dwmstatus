package main

import (
	"flag"
	"fmt"
	"io/ioutil"
	"log"
	"os/exec"
	"path/filepath"
	"sort"
	"strconv"
	"strings"
	"time"

	"github.com/BurntSushi/xgbutil"
	"github.com/BurntSushi/xgbutil/icccm"
)

var oneshot = flag.Bool("oneshot",true,"don't loop")
var wait = flag.Int("wait",30000,"waiting time")

type battery struct {
	now      float64
	full     float64
	charging bool
}

func (b battery) String() string {
	sym := "🔋"
	if b.charging {
		sym = "🔌"
	}

	return fmt.Sprintf("%v %.2f%%", sym, b.percent())
}

func (b *battery) percent() float64 {
	if !b.valid() {
		return 0
	}

	return b.now * 100.0 / b.full
}

func (b *battery) valid() bool {
	if b.now < 0 {
		return false
	}

	if b.full < 0 {
		return false
	}

	return true
}

// ByPercentage sorts batteries by their current percentage
type ByPercentage []battery

func (b ByPercentage) Len() int           { return len(b) }
func (b ByPercentage) Swap(i, j int)      { b[i], b[j] = b[j], b[i] }
func (b ByPercentage) Less(i, j int) bool { return b[i].percent() < b[j].percent() }

// possible filenames of current battery states
var batteryNowFiles = []string{"charge_now", "energy_now"}

// possible filenames of full battery states
var batteryFullFiles = []string{"charge_full", "energy_full"}

// getBatteries returns all batteries found with the glob
// /sys/class/power_supply/BAT*
// returned batteries have to be checked with the valid method.
func getBatteries() ([]battery, error) {
	batPaths, err := filepath.Glob("/sys/class/power_supply/BAT*")
	if err != nil {
		return nil, err
	}

	bs := make([]battery, len(batPaths))
	for i, batPath := range batPaths {
		bs[i].now = -1.0
		for _, v := range batteryNowFiles {
			s, err := ioutil.ReadFile(filepath.Join(batPath, v))
			if err != nil {
				continue
			}

			_, err = fmt.Sscanf(string(s), "%f\n", &bs[i].now)
			if err != nil {
				continue
			}

			break
		}

		bs[i].full = -1.0
		for _, v := range batteryFullFiles {
			s, err := ioutil.ReadFile(filepath.Join(batPath, v))
			if err != nil {
				continue
			}

			_, err = fmt.Sscanf(string(s), "%f\n", &bs[i].full)
			if err != nil {
				continue
			}

			break
		}

		s, err := ioutil.ReadFile(filepath.Join(batPath, "status"))
		if err != nil {
			continue
		}

		if strings.TrimSpace(string(s)) == "Charging" {
			bs[i].charging = true
		}
	}

	return bs, nil
}

type volume struct {
	mute   bool
	volume int
}

func (v volume) String() string {
	symbol := "🔊"
	if v.mute {
		symbol = "🔇"
	}

	return fmt.Sprintf("%v %3d%%", symbol, v.volume)
}

func getPulseaudioVolume() (*volume, error) {
	vol := &volume{}

	cmd := exec.Command("pamixer", "--get-mute")
	// just ignore the exit status because it uses it to signal muted or not.
	out, _ := cmd.Output()

	outs := string(out)
	outs = strings.TrimSpace(outs)

	if outs == "true" {
		vol.mute = true
	}

	// just ignore the exit status because it uses it to signal muted or not.
	cmd = exec.Command("pamixer", "--get-volume")
	out, _ = cmd.Output()

	outs = string(out)
	outs = strings.TrimSpace(outs)

	x, err := strconv.Atoi(outs)
	if err != nil {
		return nil, err
	}

	vol.volume = x

	return vol, nil
}

func main() {
	flag.Parse()

	xu, err := xgbutil.NewConn()
	if err != nil {
		log.Fatal(err)
	}

	w := xu.RootWin()

	for {
		t := time.Now().Format("2006-01-02 15:04")

		bs, err := getBatteries()
		if err != nil {
			log.Println(err)
		}

		// show the battery with the highest percentage
		sort.Sort(ByPercentage(bs))

		vol, err := getPulseaudioVolume()
		if err != nil {
			log.Fatal(err)
		}

		s := fmt.Sprintf("%v | %s | %s", vol, bs[len(bs)-1], t)

		err = icccm.WmNameSet(xu, w, s)
		if err != nil {
			log.Fatal(err)
		}

		if *oneshot {
			return
		}

		time.Sleep(time.Millisecond * time.Duration(*wait))
	}
}
